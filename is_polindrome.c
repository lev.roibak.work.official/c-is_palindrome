#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <string.h>

int check_num_digits(int num)
{
    int count = 0;
    do
    {
        num /= 10;
        ++count;
    } while (num != 0);

    return count;
}

int is_palindrome(int number)
{
    int len = check_num_digits(number);
    int temp = 10;
    int pow_number;
    int count = len;
    int number_from_beginning = 0;
    int number_from_end = 0;
    int half_len = len / 2;

    do
    {
        pow_number = pow(10, (len - 1));
        if (count > len)
        {
            number_from_beginning = (number / (1 * pow_number)) % (((number / (1 * pow_number)) / 10) * 10);
        }
        else
        {
            number_from_beginning = (number / (1 * pow_number));
        }
        number_from_end = (number / (temp / 10)) % (number / temp * 10);
        len--;
        temp *= 10;
        if (number_from_beginning != number_from_end)
        {
            return 1;
        }

    } while (len > half_len);

    return 0;
}

int validation_if_normal_number(int input)
{
    if (check_num_digits(input) < 2)
    {
        return 1;
    }

    return 0;
}


int main()
{
    int number;

    printf("Enter any number: ");
    if (scanf_s("%d", &number) != 1)
    {
        printf("Bruh, it is not even a normal number -_-\n");
    }else
    {
        if (validation_if_normal_number(number))
        {
            printf("The number contains only one digit, so it is obviously a palindrome, dummy :)\n");
        }
        else
        {

            if (is_palindrome(number))
            {
                printf("The number %d is not a palindrome!\n", number);
            }
            else
            {
                printf("The number %d is a palindrome!\n", number);
            }
        }
    }

    return 0;
}
